package com.example.tourtakersforguide.Notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.tourtakersforguide.Activities.GuideChatBox;
import com.example.tourtakersforguide.Activities.RequestActivity;
import com.example.tourtakersforguide.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class FirebaseMessaging extends FirebaseMessagingService {
    public static final String TAG = "FirebaseMessaging";

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        //Log.d(TAG, "onNewToken: updated token "+s);
//        SharedPreferences.Editor editor = getSharedPreferences("PREFS", MODE_PRIVATE).edit();
//        editor.putString("newToken", s);
//        editor.apply();
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            updateToken(s);
            //Toast.makeText(getApplicationContext(),"Your token updated",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "onMessageReceived: "+remoteMessage.getData());

        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isInteractive();
        //Log.e(TAG, "isScreenOn "+isScreenOn);
        if (!isScreenOn) {
            //Log.d(TAG, "wakelock: access");
            PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "TourTakers: wakeLock");
            wl.acquire(10000);
            PowerManager.WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "TourTakers:CpuLock");
            wl_cpu.acquire(10000);
        }


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        //Log.d(TAG, "onMessageReceived: called");
        String sented = remoteMessage.getData().get("sented");
        //Log.d(TAG, "onMessageReceived: sented = "+sented);
        String user = remoteMessage.getData().get("userID");
        //Log.d(TAG, "onMessageReceived: userID = "+user);
        SharedPreferences preferences = getSharedPreferences("PREFS", MODE_PRIVATE);
        String currentUser = preferences.getString("currentuser", "none");
        //Log.d(TAG, "onMessageReceived: currentUser = "+currentUser);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser != null && sented.equals(firebaseUser.getUid())) {
            if(remoteMessage.getData().get("type") != null) {
                if (remoteMessage.getData().get("type").matches("request") &&
                        remoteMessage.getData().get("startDate") != null &&
                        remoteMessage.getData().get("returnDate") != null &&
                        remoteMessage.getData().get("location") != null) {
                    DatabaseReference onlineRef = FirebaseDatabase.getInstance().getReference()
                            .child("guidesAreOnline")
                            .child(remoteMessage.getData().get("location"))
                            .child(sented)
                            .child("status");
                    onlineRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){
                                if(snapshot.getValue().equals("idle")){
                                    Intent intent = new Intent(getApplicationContext(), RequestActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("typeFor", remoteMessage.getData().get("typeFor"));
                                    bundle.putString("typeID", remoteMessage.getData().get("typeID"));
                                    bundle.putString("startDate", remoteMessage.getData().get("startDate"));
                                    bundle.putString("returnDate", remoteMessage.getData().get("returnDate"));
                                    bundle.putString("location", remoteMessage.getData().get("location"));
                                    intent.putExtras(bundle);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                            Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS |
                                            Intent.FLAG_FROM_BACKGROUND);
                                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                                            .child(remoteMessage.getData().get("typeFor"))
                                            .child(remoteMessage.getData().get("typeID"));
                                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                                            if(snapshot.exists()){
                                                startActivity(intent);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError error) {

                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }else {
                if (!currentUser.equals(user) && currentUser.equals("none")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        sendOreoNotification(remoteMessage);
                        //Log.d(TAG, "onMessageReceived: send oreo");
                    } else {
                        sendNotification(remoteMessage);
                        //Log.d(TAG, "onMessageReceived: send normal");
                    }
                }
            }
        }
    }

    private void updateToken(String refreshToken) {
        Token token1 = new Token(refreshToken);
        ArrayList<String> eventIDs = new ArrayList<>();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference userActivity = FirebaseDatabase.getInstance().getReference().child("userActivities").child(user.getUid()).child("events");
        userActivity.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        HashMap<String, Object> data = (HashMap<String, Object>) childSnapshot.getValue();
                        eventIDs.add((String) data.get("eventID"));
                    }
                    for (int i = 0; i < eventIDs.size(); i++) {
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("eventCommentsTokens");
                        HashMap<String, Object> hashMap = new HashMap<>();
                        hashMap.put("userID", user.getUid());
                        hashMap.put("token", token1.getToken());
                        ref.child(eventIDs.get(i)).child(user.getUid()).setValue(hashMap);
                    }
                    DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("profile").child(user.getUid());
                    userRef.child("token").setValue(token1.getToken()).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            SharedPreferences.Editor editor = getSharedPreferences("PREFS", MODE_PRIVATE).edit();
                            editor.putString("newToken", refreshToken);
                            editor.apply();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendOreoNotification(RemoteMessage remoteMessage) {
        String userID = remoteMessage.getData().get("userID");
        String icon = remoteMessage.getData().get("icon");
        String title = remoteMessage.getData().get("title");
        String body = remoteMessage.getData().get("body");
        String eventID = remoteMessage.getData().get("eventID");
        String from = remoteMessage.getData().get("fromActivity");
        String sex = remoteMessage.getData().get("userSex");
        Bitmap bitmap = null;
        Log.d(TAG, "sendOreoNotification: "+remoteMessage.getData().get("fromActivity"));
        if (remoteMessage.getData().get("userImage").trim().length() != 0) {
            bitmap = getCroppedBitmap(getBitmapFromURL(remoteMessage.getData().get("userImage")));
        } else {
            if (sex.equals("male")) {
                bitmap = getCroppedBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.man));
            } else if (sex.equals("female")) {
                bitmap = getCroppedBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.woman));
            }
        }
        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference().child("event").child(eventID);
        Bitmap finalBitmap = bitmap;
        eventRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    HashMap<String, Object> hashMap = (HashMap<String, Object>) dataSnapshot.getValue();
                    String eventPlace = (String) hashMap.get("place");
                    RemoteMessage.Notification notification = remoteMessage.getNotification();
                    int j = Integer.parseInt(userID.replaceAll("[\\D]", ""));
                    Log.d(TAG, "onDataChange: "+from);
                    if (from.matches("guideChat")) {
                        Intent intent = new Intent(getApplicationContext(), GuideChatBox.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("eventId", eventID);
                        bundle.putString("chatPartnerID", userID);
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), j, intent, PendingIntent.FLAG_ONE_SHOT);
                        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                        OreoNotification oreoNotification = new OreoNotification(getApplicationContext());
                        Notification.Builder builder = oreoNotification.getOreoNotification(title + ": " + eventPlace, body, pendingIntent,
                                defaultSound, icon, finalBitmap);

                        int i = 0;
                        if (j > 0) {
                            i = j;
                        }

                        oreoNotification.getManager().notify(i, builder.build());
                    }
                }else {
                    DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference().child("tour").child(eventID);
                    eventRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()) {
                                HashMap<String, Object> hashMap = (HashMap<String, Object>) snapshot.getValue();
                                Log.d(TAG, "onDataChange: "+hashMap);
                                String eventPlace = (String) hashMap.get("place");
                                RemoteMessage.Notification notification = remoteMessage.getNotification();
                                int j = Integer.parseInt(userID.replaceAll("[\\D]", ""));
                                Log.d(TAG, "onDataChange: "+from);
                                if (from.matches("guideChat")) {
                                    Intent intent = new Intent(getApplicationContext(), GuideChatBox.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("eventId", eventID);
                                    bundle.putString("chatPartnerID", userID);
                                    intent.putExtras(bundle);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), j, intent, PendingIntent.FLAG_ONE_SHOT);
                                    Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                                    OreoNotification oreoNotification = new OreoNotification(getApplicationContext());
                                    Notification.Builder builder = oreoNotification.getOreoNotification(title + ": " + eventPlace, body, pendingIntent,
                                            defaultSound, icon, finalBitmap);

                                    int i = 0;
                                    if (j > 0) {
                                        i = j;
                                    }

                                    oreoNotification.getManager().notify(i, builder.build());
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    private void sendNotification(RemoteMessage remoteMessage) {

        String eventID = remoteMessage.getData().get("eventID");
        String icon = remoteMessage.getData().get("icon");
        String title = remoteMessage.getData().get("title");
        String body = remoteMessage.getData().get("body");
        String userID = remoteMessage.getData().get("userID");
        String from = remoteMessage.getData().get("fromActivity");
        String sex = remoteMessage.getData().get("userSex");
        Bitmap bitmap = null;
        if (remoteMessage.getData().get("userImage").trim().length() != 0) {
            bitmap = getCroppedBitmap(getBitmapFromURL(remoteMessage.getData().get("userImage")));
        } else {
            if (sex.equals("male")) {
                bitmap = getCroppedBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.man));
            } else if (sex.equals("female")) {
                bitmap = getCroppedBitmap(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.woman));
            }
        }
        Log.d(TAG, "sendNotification: userID = "+userID);
        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference().child("event").child(eventID);
        Bitmap finalBitmap = bitmap;
        eventRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    HashMap<String, Object> hashMap = (HashMap<String, Object>) dataSnapshot.getValue();
                    String eventPlace = (String) hashMap.get("place");
                    RemoteMessage.Notification notification = remoteMessage.getNotification();
                    int j = Integer.parseInt(userID.replaceAll("[\\D]", ""));
                    if (from.matches("guideChat")) {
                        Intent intent = new Intent(getApplicationContext(), GuideChatBox.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("eventId", eventID);
                        bundle.putString("chatPartnerID", userID);
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), j, intent, PendingIntent.FLAG_ONE_SHOT);
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "Events")
                                .setSmallIcon(R.drawable.ic_stat_notification)
                                .setLargeIcon(finalBitmap)
                                .setContentTitle(title + ": " + eventPlace)
                                .setContentText(body)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .setAutoCancel(true)
                                .setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.swiftly))
                                .setContentIntent(pendingIntent);
                        NotificationManager noti = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                        int i = 0;
                        if (j > 0) {
                            i = j;
                        }

                        noti.notify(i, builder.build());
                    }
                }else {
                    DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference().child("tour").child(eventID);
                    eventRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (dataSnapshot.exists()) {
                                HashMap<String, Object> hashMap = (HashMap<String, Object>) dataSnapshot.getValue();
                                String eventPlace = (String) hashMap.get("place");
                                RemoteMessage.Notification notification = remoteMessage.getNotification();
                                int j = Integer.parseInt(userID.replaceAll("[\\D]", ""));
                                if (from.matches("guideChat")) {
                                    Intent intent = new Intent(getApplicationContext(), GuideChatBox.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("eventId", eventID);
                                    bundle.putString("chatPartnerID", userID);
                                    intent.putExtras(bundle);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), j, intent, PendingIntent.FLAG_ONE_SHOT);
                                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "Events")
                                            .setSmallIcon(R.drawable.ic_stat_notification)
                                            .setLargeIcon(finalBitmap)
                                            .setContentTitle(title + ": " + eventPlace)
                                            .setContentText(body)
                                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                                            .setAutoCancel(true)
                                            .setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.swiftly))
                                            .setContentIntent(pendingIntent);
                                    NotificationManager noti = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                                    int i = 0;
                                    if (j > 0) {
                                        i = j;
                                    }

                                    noti.notify(i, builder.build());
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
