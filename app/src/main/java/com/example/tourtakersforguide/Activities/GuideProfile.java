package com.example.tourtakersforguide.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.tourtakersforguide.Adapter.Compliment_adapter;
import com.example.tourtakersforguide.Internet.ConnectivityReceiver;
import com.example.tourtakersforguide.Model.Compliment_model;
import com.example.tourtakersforguide.Model.Profile;
import com.example.tourtakersforguide.Profile_BottomSheet;
import com.example.tourtakersforguide.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class GuideProfile extends AppCompatActivity {

    private LinearLayout ratingLayout, totalTourLayout, totalEventLayout;
    private TextView profilename, profileemail, profilephoneno, totalTourTV, ratingTV, totalEventTV, n2, p2;
    private CardView phoneUpdate, nameUpdate;
    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private String userId;
    private String name;
    private String email;
    private String phone;
    private String image;
    private Float rating;
    private int ratingCounter;
    private int totalTour;
    private int totalEvent;
    private StorageReference storageReference;
    private FirebaseStorage storage;
    private Uri imageUri;
    private ImageView profileImage, n1, e2, e1, p1;
    private Snackbar snackbar;
    private ConnectivityReceiver connectivityReceiver;
    private IntentFilter intentFilter;
    private RecyclerView compliment_recycler;
    private Compliment_adapter compliment_adapter;
    private List<Compliment_model> compliment_models;
    ProgressBar progressBar;
    Animation topAnim, bottomAnim2, leftAnim, rightAnim, fadeIn, scaleAnim, ball3Anim, edittext_anim, blink;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_profile);

        init();



        registerReceiver(connectivityReceiver, intentFilter);
        userId = auth.getUid();
        animation();
        DatabaseReference getRef = reference.child(userId);

        getRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Profile profile = dataSnapshot.getValue(Profile.class);

                name = profile.getName();
                email = profile.getEmail();
                phone = profile.getPhone();
                image = profile.getImage();
                totalTour = profile.getTour();
                totalEvent = profile.getEvent();
                rating = profile.getRating();
                ratingCounter = profile.getRatingCounter();
                DecimalFormat df = new DecimalFormat("0.0");
                float averageRating = 0;
                if(rating!=0){
                    averageRating = rating / ratingCounter;
                }

                progressBar.setVisibility(View.GONE);

                profilename.setText(name);
                profileemail.setText(email);
                profilephoneno.setText(phone);
                totalEventTV.setText(String.valueOf(totalEvent));
                totalTourTV.setText(String.valueOf(totalTour));
                ratingTV.setText(String.valueOf(df.format(averageRating)));

                if (!image.isEmpty()) {
                    Glide.with(GuideProfile.this)
                            .load(image)
                            .fitCenter()
                            .into(profileImage);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        YoYo.with(Techniques.FadeIn)
                .duration(1700)
                .repeat(0)
                .playOn(profilename);

        YoYo.with(Techniques.FadeIn)
                .duration(1700)
                .repeat(0)
                .playOn(profileemail);

        YoYo.with(Techniques.FadeIn)
                .duration(1700)
                .repeat(0)
                .playOn(profilephoneno);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setFixAspectRatio(true)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .start(GuideProfile.this);
            }
        });


        nameUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Bundle args = new Bundle();
                    args.putString("name", "name");
                    args.putString("id", userId);
                    args.putString("nameForHint", name);
                    Profile_BottomSheet bottom_sheet = new Profile_BottomSheet();
                    bottom_sheet.setArguments(args);
                    bottom_sheet.show(getSupportFragmentManager(), "bottomSheet");

            }
        });

        phoneUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Bundle args = new Bundle();
                    args.putString("phone", "phone");
                    args.putString("id", userId);
                    args.putString("phoneForHint", phone);
                    Profile_BottomSheet bottom_sheet = new Profile_BottomSheet();
                    bottom_sheet.setArguments(args);
                    bottom_sheet.show(getSupportFragmentManager(), "bottomSheet");
            }
        });

        compliment_models.add(new Compliment_model(R.drawable.starrating,"6-star rating"));
        compliment_models.add(new Compliment_model(R.drawable.like,"Great Attitude"));
        compliment_models.add(new Compliment_model(R.drawable.conversation,"Great Conversation"));
        compliment_models.add(new Compliment_model(R.drawable.expertnavigation,"Expert Navigation"));
        compliment_models.add(new Compliment_model(R.drawable.upload_user_photo,"Great Amenities"));
        compliment_models.add(new Compliment_model(R.drawable.clean,"Neat & Clean"));

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK){
                Uri resultUri = result.getUri();
                imageUri = resultUri;
                progressBar.setVisibility(View.VISIBLE);
                if (!image.isEmpty()){
                    deleteImage();
                }
                else{
                    uploadImage();
                }
            }
            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getApplicationContext(),"Failed",Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void uploadImage() {
        if (imageUri != null){
            StorageReference imgRef = storageReference.child("guideProfileImage/" + userId);
            imgRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    storageReference.child("guideProfileImage/" + userId).getDownloadUrl()
                            .addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            imageUri = uri;
                            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("GuideProfile").child(userId).child("image");
                            databaseReference.setValue(imageUri.toString());
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Toasty.error(GuideProfile.this, "Image upload failed!" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot
                            .getTotalByteCount();
                    if(progress<100){
                        Toasty.info(getApplicationContext(),"Uploading...",Toasty.LENGTH_SHORT).show();
                        //Toasty.info(getApplicationContext(),"Uploaded " + (int) progress + "% done",Toasty.LENGTH_SHORT).show();
                    }else {
                        Toasty.success(getApplicationContext(),"Successfully uploaded",Toasty.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void deleteImage() {
        storageReference.child("guideProfileImage/" + userId).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                StorageReference photoRef = FirebaseStorage.getInstance().getReferenceFromUrl(uri.toString());
                photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        uploadImage();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        progressBar.setVisibility(View.GONE);
                        Toasty.error(getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    private void animation() {
        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnim2 = AnimationUtils.loadAnimation(this, R.anim.bottom_animation2);
        leftAnim = AnimationUtils.loadAnimation(this, R.anim.left_animation);
        rightAnim = AnimationUtils.loadAnimation(this, R.anim.right_animation);
        fadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);
        scaleAnim = AnimationUtils.loadAnimation(this, R.anim.scale_anim);
        ball3Anim = AnimationUtils.loadAnimation(this, R.anim.ball3_animation);
        edittext_anim = AnimationUtils.loadAnimation(this, R.anim.edittext_anim);
        blink = AnimationUtils.loadAnimation(this, R.anim.blink_anim);

        e1.setAnimation(leftAnim);
        e2.setAnimation(rightAnim);
        n1.setAnimation(leftAnim);
        n2.setAnimation(rightAnim);
        p1.setAnimation(leftAnim);
        p2.setAnimation(rightAnim);
        totalEventLayout.setAnimation(bottomAnim2);
        totalTourLayout.setAnimation(bottomAnim2);
        ratingLayout.setAnimation(bottomAnim2);
    }

    private void init() {
        ratingLayout = findViewById(R.id.ratingLayout);
        totalTourLayout = findViewById(R.id.totalTourLayout);
        totalEventLayout = findViewById(R.id.totalEventLayout);
        totalTourTV = findViewById(R.id.totalTour);
        ratingTV = findViewById(R.id.rating);
        totalEventTV = findViewById(R.id.totalEvent);
        nameUpdate = findViewById(R.id.nameUpdate);
        phoneUpdate = findViewById(R.id.phoneUpdate);
        profilename = findViewById(R.id.profileusername);
        profileemail = findViewById(R.id.profileemail);
        profilephoneno = findViewById(R.id.profilephoneNo);
        progressBar = findViewById(R.id.progressBar);
        profileImage = findViewById(R.id.profileIV);
        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference("GuideProfile");
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        n1 = findViewById(R.id.n1);
        n2 = findViewById(R.id.n2);
        e1 = findViewById(R.id.e1);
        e2 = findViewById(R.id.e2);
        p1 = findViewById(R.id.p1);
        p2 = findViewById(R.id.p2);
        intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        connectivityReceiver = new ConnectivityReceiver();
        storageReference = FirebaseStorage.getInstance().getReference();
        compliment_recycler =findViewById(R.id.complimentRecycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        compliment_recycler.setLayoutManager(layoutManager);
        compliment_models = new ArrayList<>();
        compliment_adapter = new Compliment_adapter(this,compliment_models);
        compliment_recycler.setAdapter(compliment_adapter);

    }
}
