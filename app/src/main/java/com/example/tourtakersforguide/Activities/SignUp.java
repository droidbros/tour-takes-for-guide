package com.example.tourtakersforguide.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tourtakersforguide.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.dmoral.toasty.Toasty;

public class SignUp extends AppCompatActivity {
    ImageView nidverify;
    private EditText emailEt, nameEt, phoneNoEt, passwordEt;
    private Button signupBtn;
    private TextView txt1,nidtext;
    private FirebaseAuth auth;
    private DatabaseReference reference;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private String email, name, phone, password, image, rating,location;
    Animation topAnim, bottomAnim, leftAnim, rightAnim, ball1Anim, ball2Anim, ball3Anim, edittext_anim;
    private Uri uri;
    private String userId;
    private ProgressDialog progressDialog;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private Bitmap imageBitmap;
    private Spinner locationSpinner;
    private String[] locationList = {"Choose your location","Bandarban","Chittagong","Cox's Bazar","Khagrachari","Moulvibazar","Rangamati","Sunamganj","Sylhet"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();
        animation();

        setLocationSpinnerValue();

        nidverify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent,1);
            }
        });

        signupBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                email = emailEt.getText().toString();
                name = nameEt.getText().toString();
                phone = phoneNoEt.getText().toString();
                password = passwordEt.getText().toString();
                location = locationSpinner.getSelectedItem().toString();


                if (TextUtils.isEmpty(email)) {
                    emailEt.setError("Enter email");
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    emailEt.setError("Enter valid email");
                    emailEt.requestFocus();
                } else if (TextUtils.isEmpty(name)) {
                    nameEt.setError("Enter User name");
                    nameEt.requestFocus();
                } else if (TextUtils.isEmpty(phone)) {
                    phoneNoEt.setError("Enter phone No.");
                    phoneNoEt.requestFocus();
                } else if (TextUtils.isEmpty(password)) {
                    passwordEt.setError("Enter Password!");
                    passwordEt.requestFocus();
                } else if (passwordEt.length() < 6) {
                    passwordEt.setError("At least 6 characters!", null);
                    passwordEt.requestFocus();
                }
                else if (nidverify.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.nid).getConstantState())){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        nidtext.setTextColor(getColor(R.color.Red));
                    }
                    nidtext.setError("Please upload your NID card!");
                }
                else if(locationSpinner.getSelectedItem().equals("Choose your location")){
                    Toast.makeText(SignUp.this, "Please select your zone!", Toast.LENGTH_SHORT).show();
                    locationSpinner.requestFocus();
                }
                else {
                    signupBtn.setEnabled(false);
                    checkmail();
                }
            }
        });
    }

    private void uploadNID() {

        Log.d("uri",uri.toString());
            if (uri!=null){
            StorageReference nidRef = storageReference.child("nidImage/"+userId);
            nidRef.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                   if (task.isSuccessful()){
                       Toasty.success(getApplicationContext(),"Image Uploaded");
                   }
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    image = storageReference.child("nidImage/"+userId).getDownloadUrl().toString();
                    Log.d("image",image);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(SignUp.this, ""+e.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }

    }

    private void checkmail() {
        email = emailEt.getText().toString();
        auth.fetchSignInMethodsForEmail(email)
                .addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                    @Override
                    public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                        boolean check = !task.getResult().getSignInMethods().isEmpty();
                        if (!check) {
                            //Toast.makeText(getApplicationContext(),"Email not found",Toast.LENGTH_SHORT).show();
                            signup(email, name, phone, password,image,location);
                        } else {
                            signupBtn.setEnabled(true);
                            emailEt.setError("This email address is already in use by another account!");
                            emailEt.requestFocus();
                        }
                    }
                });
    }

    private void signup(final String email, final String name, final String phone, final String password, String image,final String location) {
        progressDialog.setTitle("Signing up...");
        progressDialog.show();

        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                signupBtn.setEnabled(true);
                if (task.isSuccessful()){

                    userId = auth.getCurrentUser().getUid();

                    uploadNID();

                   String imageUrl = storageReference.child("nidImage/"+userId).getDownloadUrl().toString();
                   Log.d("imageUrl",imageUrl);

                    DatabaseReference dataref = reference.child("GuideProfile").child(userId);

                    HashMap<String, Object> userInfo = new HashMap<>();

                    userInfo.put("name", name);
                    userInfo.put("email", email);
                    userInfo.put("phone", phone);
                    userInfo.put("password", password);
                    userInfo.put("location", location);
                    userInfo.put("NID", imageUrl);
                    userInfo.put("image", "");
                    userInfo.put("rating", 0.0);
                    userInfo.put("ratingCounter", 0);
                    userInfo.put("tour", 0);
                    userInfo.put("event", 0);
                    userInfo.put("Id", userId);

                    dataref.setValue(userInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                auth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            progressDialog.dismiss();
                                            Toasty.success(getApplicationContext(), "Successfully Signed Up.", Toast.LENGTH_SHORT).show();
                                            Toasty.Config.getInstance().setTextSize(14).allowQueue(true).apply();
                                            Toasty.info(getApplicationContext(), "Please check your mailbox for verification!", Toast.LENGTH_LONG).show();
                                            Toasty.Config.reset();
                                            startActivity(new Intent(SignUp.this, SignIn.class).putExtra("email", email).putExtra("userId",userId));
                                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                        } else {
                                            Toast.makeText(getApplicationContext(), task.getException().toString(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });

                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toasty.success(getApplicationContext(),e.getMessage());
                        }
                    });
                }
            }
        });
    }


    private void animation() {

        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);
        leftAnim = AnimationUtils.loadAnimation(this, R.anim.left_animation);
        rightAnim = AnimationUtils.loadAnimation(this, R.anim.right_animation);
        ball1Anim = AnimationUtils.loadAnimation(this, R.anim.ball1_animation);
        ball2Anim = AnimationUtils.loadAnimation(this, R.anim.ball2_animation);
        ball3Anim = AnimationUtils.loadAnimation(this, R.anim.ball3_animation);
        edittext_anim = AnimationUtils.loadAnimation(this, R.anim.edittext_anim);


        txt1.setAnimation(topAnim);
        signupBtn.setAnimation(bottomAnim);
        emailEt.setAnimation(edittext_anim);
    }

    private void init() {
        nidverify = findViewById(R.id.nidverify);
        emailEt = findViewById(R.id.email_ET);
        nameEt = findViewById(R.id.name_ET);
        phoneNoEt = findViewById(R.id.phoneEt);
        passwordEt = findViewById(R.id.password_ET);
        signupBtn = findViewById(R.id.signup_BTN);
        txt1 = findViewById(R.id.txt1);
        nidtext = findViewById(R.id.nidText);
        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        locationSpinner = findViewById(R.id.locationSpinner);

        progressDialog = new ProgressDialog(this);

        Intent intent = getIntent();
        email = intent.getExtras().getString("email");
        emailEt.setText(email);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode==RESULT_OK){
            if (requestCode==1){
                uri = data.getData();
                nidverify.setImageURI(uri);
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
    private void setLocationSpinnerValue() {
        ArrayAdapter<String> areasAdapter = new ArrayAdapter<String>(SignUp.this, R.layout.spinner_item, locationList);
        areasAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(areasAdapter);

    }

}
