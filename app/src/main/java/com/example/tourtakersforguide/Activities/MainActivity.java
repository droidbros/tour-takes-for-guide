package com.example.tourtakersforguide.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.tourtakersforguide.ForGPS.AppConstants;
import com.example.tourtakersforguide.ForGPS.GpsUtils;
import com.example.tourtakersforguide.Fragments.Earnings;
import com.example.tourtakersforguide.Fragments.LoaderFragment;
import com.example.tourtakersforguide.Fragments.ServiceFragment;
import com.example.tourtakersforguide.Fragments.TourFragment;
import com.example.tourtakersforguide.Fragments.WeatherFragment;
import com.example.tourtakersforguide.Internet.ConnectivityReceiver;
import com.example.tourtakersforguide.Model.Profile;
import com.example.tourtakersforguide.Notifications.Token;
import com.example.tourtakersforguide.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    public static final String TAG = "MainActivity";
    boolean doubleBackToExitPressedOnce = false;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private Toast toast = null;
    private Snackbar snackbar;
    private ConnectivityReceiver connectivityReceiver;
    private IntentFilter intentFilter;
    private DatabaseReference reference;
    private StorageReference storageReference;
    private FirebaseAuth auth;
    private String userId, name, email, image, currentFragment;
    private Uri imageUri;
    private Float rating;
    private int ratingCounter;
    private ImageView circularImageView;
    private TextView UserName, userEmail, navRating;
    private LinearLayout ratingLaout;
    private boolean isContinue = false;
    private boolean isGPS = false;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private String userID, userLocation;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocale();
        setContentView(R.layout.activity_main);
        init();
        setNewToken();
        Handler handler = new Handler();
        Runnable checkOverlaySetting = new Runnable() {
            @Override
            @TargetApi(23)
            public void run() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    return;
                }
                if (Settings.canDrawOverlays(MainActivity.this)) {
                    //You have the permission, re-launch MainActivity
                    Intent i = new Intent(MainActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    return;
                }
                handler.postDelayed(this, 1000);
            }
        };
        if (!Settings.canDrawOverlays(this)) {
            AlertDialog alertDialog;
            AlertDialog.Builder dialog = new AlertDialog.Builder(this,R.style.MyDialogTheme);
            dialog.setTitle("Required!");
            dialog.setMessage("You have to allow display over other apps for better experience! ");
            dialog.setCancelable(false);
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 0);
                    handler.postDelayed(checkOverlaySetting, 1000);
                    dialog.dismiss();
                }
            });
            alertDialog = dialog.create();
            alertDialog.show();
        }
        battaryOptimization();
        new GpsUtils(this).turnGPSOn(new GpsUtils.onGpsListener() {
            @Override
            public void gpsStatus(boolean isGPSEnable) {
                // turn on GPS
                isGPS = isGPSEnable;
            }
        });
        getGuideInfo(new TourFragment.guideInfoCallback() {
            @Override
            public void guideID(String ID) {
                userID = ID;
            }

            @Override
            public void guideLocation(String location) {
                userLocation = location;
            }
        });
        if (savedInstanceState == null) {
            if((getIntent().getStringExtra("startDate") != null) && (getIntent().getStringExtra("returnDate") != null)){
                //Log.d(TAG, "onCreate: pass");
                Bundle bundle = new Bundle();
                bundle.putString("forView",getIntent().getStringExtra("forView"));
                bundle.putString("startDate",getIntent().getStringExtra("startDate"));
                bundle.putString("returnDate",getIntent().getStringExtra("returnDate"));
                TourFragment tourFragment = new TourFragment();
                tourFragment.setArguments(bundle);
                FragmentTransaction tour = getSupportFragmentManager().beginTransaction();
                tour.replace(R.id.fragment_container, tourFragment);
                tour.commit();
                navigationView.getMenu().getItem(0).setChecked(true);
            }else {
                FragmentTransaction tour = getSupportFragmentManager().beginTransaction();
                tour.replace(R.id.fragment_container, new LoaderFragment());
                tour.commit();
                navigationView.getMenu().getItem(0).setChecked(true);
            }
        }
        userId = auth.getUid();

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        if (!isContinue) {
                            Log.d(TAG, "onLocationResult: not continue");
                            //findweather(wayLatitude,wayLongitude);
                        } else {
                            if((userID != null) && (userLocation != null)) {
                                DatabaseReference onlineRef = FirebaseDatabase.getInstance().getReference().child("guidesAreOnline").child(userLocation).child(userID);
                                onlineRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            onlineRef.child("currentLocationLatitude").setValue(wayLatitude);
                                            onlineRef.child("currentLocationLongitude").setValue(wayLongitude);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                        if (!isContinue && mFusedLocationClient != null) {
                            Log.d(TAG, "onLocationResult: fused");
                            mFusedLocationClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };

        if (!isGPS) {
            Toasty.info(getApplicationContext(), "Please turn on your GPS", Toasty.LENGTH_SHORT).show();
        }
        isContinue = true;
        getLocation();

        DatabaseReference userRef = reference.child(userId);
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Profile profile = dataSnapshot.getValue(Profile.class);

                try {
                    name = profile.getName();
                    email = profile.getEmail();
                    image = profile.getImage();
                    rating = profile.getRating();
                    ratingCounter = profile.getRatingCounter();
                    DecimalFormat df = new DecimalFormat("0.0");
                    float averageRating = 0;
                    if(rating!=0){
                        averageRating = rating / ratingCounter;
                    }

                    UserName.setText(name);
                    userEmail.setText(email);
                    navRating.setText(String.valueOf(df.format(averageRating)));
                    if (!image.isEmpty()) {
                        try {
                            Glide.with(MainActivity.this)
                                    .load(image)
                                    .fitCenter()
                                    .into(circularImageView);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Toasty.error(getApplicationContext(), "Data has changed!", Toasty.LENGTH_LONG).show();
                    FirebaseAuth.getInstance().signOut();
                    startActivity(new Intent(MainActivity.this, SignIn.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        circularImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Pair[] pairs = new Pair[2];
                    pairs[0] = new Pair<View, String>(circularImageView, "imageTransition");
                    pairs[1] = new Pair<View, String>(ratingLaout, "ratingTransition");
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                    startActivity(new Intent(MainActivity.this, GuideProfile.class), options.toBundle());
                } else {
                    // Swap without transition
                    startActivity(new Intent(MainActivity.this, GuideProfile.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        });

    }
    private void battaryOptimization() {
        if (checkBatteryOptimized()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
            builder.setMessage("Please ignore Battery Optimization for working this app properly.")
                    .setCancelable(true)
                    .setTitle("Warning...")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            startBatteryOptimizeDialog();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }
    }

    /**
     * return false if in settings "Not optimized" and true if "Optimizing battery use"
     */
    private boolean checkBatteryOptimized() {
        final PowerManager pwrm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return !pwrm.isIgnoringBatteryOptimizations(getBaseContext().getPackageName());
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    @SuppressLint("BatteryLife")
    private void startBatteryOptimizeDialog() {
        try {
            Intent intent;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                startActivity(intent);
            }


        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000); // 30 seconds
        locationRequest.setFastestInterval(15 * 1000); // 20 seconds
        intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        connectivityReceiver = new ConnectivityReceiver();
        toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        reference = FirebaseDatabase.getInstance().getReference("GuideProfile");
        navigationView.setNavigationItemSelectedListener(this);
        auth = FirebaseAuth.getInstance();
        circularImageView = navigationView.getHeaderView(0).findViewById(R.id.navImageView);
        UserName = navigationView.getHeaderView(0).findViewById(R.id.namefromNavigation);
        userEmail = navigationView.getHeaderView(0).findViewById(R.id.email_fromNavigation);
        navRating = navigationView.getHeaderView(0).findViewById(R.id.navRating);
        ratingLaout = navigationView.getHeaderView(0).findViewById(R.id.ratingLayout);
        storageReference = FirebaseStorage.getInstance().getReference();
    }

    public interface guideInfoCallback{
        void guideID(String ID);
        void guideLocation(String location);
    }

    private void setNewToken(){
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                updateToken(instanceIdResult.getToken());
            }
        });
    }

    private void updateToken(String token) {
        if (auth.getUid() != null) {
            Token token1 = new Token(token);
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("GuideProfile").child(auth.getUid());
            userRef.child("token").setValue(token1.getToken()).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    //preventMultiDeviceLogin();
                }
            });
        }
    }

    private void getGuideInfo(TourFragment.guideInfoCallback infoCallback){
        DatabaseReference infoRef = FirebaseDatabase.getInstance().getReference().child("GuideProfile").child(auth.getUid());
        infoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    HashMap<String, Object> map = (HashMap<String, Object>) dataSnapshot.getValue();
                    infoCallback.guideID((String) map.get("Id"));
                    infoCallback.guideLocation((String) map.get("location"));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    AppConstants.LOCATION_REQUEST);

        } else {
            if (isContinue) {
                Log.d(TAG, "getLocation: reached continue");
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            } else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            wayLatitude = location.getLatitude();
                            wayLongitude = location.getLongitude();
                        } else {
                            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                        }
                    }
                });
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1000) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (isContinue) {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    } else {
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    wayLatitude = location.getLatitude();
                                    wayLongitude = location.getLongitude();
                                } else {
                                    mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                                }
                            }
                        });
                    }

            } else {
                Toasty.error(getApplicationContext(), "Location permission denied!", Toasty.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.tour:
                FragmentTransaction tour = getSupportFragmentManager().beginTransaction();
                tour.replace(R.id.fragment_container, new TourFragment());
                tour.commit();
                drawerLayout.closeDrawers();
                navigationView.getMenu().getItem(0).setChecked(true);
                navigationView.getMenu().getItem(1).setChecked(false);
                navigationView.getMenu().getItem(2).setChecked(false);
                navigationView.getMenu().getItem(3).setChecked(false);
                break;

            case R.id.earning:
                FragmentTransaction earn = getSupportFragmentManager().beginTransaction();
                earn.replace(R.id.fragment_container, new Earnings());
                earn.commit();
                drawerLayout.closeDrawers();
                navigationView.getMenu().getItem(1).setChecked(true);
                navigationView.getMenu().getItem(0).setChecked(false);
                navigationView.getMenu().getItem(2).setChecked(false);
                navigationView.getMenu().getItem(3).setChecked(false);
                break;
            case R.id.service:
                FragmentTransaction service = getSupportFragmentManager().beginTransaction();
                service.replace(R.id.fragment_container, new ServiceFragment());
                service.commit();
                drawerLayout.closeDrawers();
                navigationView.getMenu().getItem(2).setChecked(true);
                navigationView.getMenu().getItem(0).setChecked(false);
                navigationView.getMenu().getItem(1).setChecked(false);
                navigationView.getMenu().getItem(3).setChecked(false);
                break;

            case R.id.weather:
                FragmentTransaction weather = getSupportFragmentManager().beginTransaction();
                weather.replace(R.id.fragment_container, new WeatherFragment());
                weather.commit();
                drawerLayout.closeDrawers();
                navigationView.getMenu().getItem(3).setChecked(true);
                navigationView.getMenu().getItem(0).setChecked(false);
                navigationView.getMenu().getItem(2).setChecked(false);
                navigationView.getMenu().getItem(1).setChecked(false);
                break;

            case R.id.logout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, SignIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                break;
            case R.id.terms:
                drawerLayout.closeDrawers();
                break;
            case R.id.settings:
                drawerLayout.closeDrawers();
                Intent i = new Intent(MainActivity.this, Language.class);
                startActivityForResult(i, 1);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
        }
        return false;
    }
    private void hideKeyboardFrom(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    private void loadLocale() {
        SharedPreferences preferences = getSharedPreferences("com.example.tourtakersforguide.Activities", Context.MODE_PRIVATE);
        String language = preferences.getString("My_Lang", "");

        if (language.equals("en")) {
            setLanguage("en");

        } else if (language.equals("bn")) {
            setLanguage("bn");
        }

    }

    private void setLanguage(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.activity_main);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Double Tap For Close App", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentTransaction tour = getSupportFragmentManager().beginTransaction();
        tour.replace(R.id.fragment_container, new TourFragment());
        tour.commit();
        navigationView.getMenu().getItem(0).setChecked(true);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        recreate();
    }
}
