package com.example.tourtakersforguide.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tourtakersforguide.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

import es.dmoral.toasty.Toasty;

public class SignInGrantAccess extends AppCompatActivity {

    private EditText passEt;
    private Button singin;
    private String email, password,userId;
    String activeStatus;
    private TextView txt1;
    private ImageView logo;
    private FirebaseAuth auth;
    private DatabaseReference reference;
    Animation topAnim, bottomAnim, leftAnim, rightAnim, ball1Anim, ball2Anim, ball3Anim, edittext_anim, blink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_grant_access);

        init();
        animation();

        singin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                email = intent.getStringExtra("email");
                password = passEt.getText().toString();
                if (TextUtils.isEmpty(password)) {
                    passEt.setError("Please enter password!", null);
                } else if (passEt.length() < 6) {
                    passEt.setError("At least 6 characters!", null);
                } else {
                    singin.setText("Connecting");
                    logo.startAnimation(blink);
                    singin.setEnabled(false);
                    signinuser(email, password);
                }
            }
        });
    }

    private void signinuser(String email, String password) {


        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                singin.setEnabled(true);
                if (task.isSuccessful()) {
                    logo.clearAnimation();
                    if (auth.getCurrentUser().isEmailVerified()) {
                        singin.setText("Login");
                        final String ID = auth.getCurrentUser().getUid();
                        DatabaseReference showref = reference.child(ID);
                        showref.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    //unregisterReceiver(connectivityReceiver);

                                    Intent intent = new Intent(SignInGrantAccess.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    finish();
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    startActivity(intent);


                                } else {
                                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("profile").child(ID);
                                    ref.addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if(dataSnapshot.exists()){
                                                Toasty.error(getApplicationContext(), "You can't use tourist account to this app!", Toasty.LENGTH_SHORT).show();
                                                FirebaseAuth.getInstance().signOut();
                                                Intent intent = new Intent(SignInGrantAccess.this, SignIn.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                            }else {
                                                Toasty.error(getApplicationContext(),"Sign In failed. Please contact to the support!",Toasty.LENGTH_LONG).show();
                                                FirebaseAuth.getInstance().signOut();
                                                Intent intent = new Intent(SignInGrantAccess.this, SignIn.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    } else {
                        singin.setText("Login");
                        Toasty.info(getApplicationContext(), "Please verify your email address!", Toasty.LENGTH_LONG).show();
                    }

                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                singin.setText("Login");
                logo.clearAnimation();
                Toast.makeText(SignInGrantAccess.this, "" + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

    private void animation() {
        topAnim = AnimationUtils.loadAnimation(this, R.anim.top_animation);
        bottomAnim = AnimationUtils.loadAnimation(this, R.anim.bottom_animation);
        leftAnim = AnimationUtils.loadAnimation(this, R.anim.left_animation);
        rightAnim = AnimationUtils.loadAnimation(this, R.anim.right_animation);
        ball1Anim = AnimationUtils.loadAnimation(this, R.anim.ball1_animation);
        ball2Anim = AnimationUtils.loadAnimation(this, R.anim.ball2_animation);
        ball3Anim = AnimationUtils.loadAnimation(this, R.anim.ball3_animation);
        edittext_anim = AnimationUtils.loadAnimation(this, R.anim.edittext_anim);
        blink = AnimationUtils.loadAnimation(this, R.anim.blink_anim);

        logo.setAnimation(leftAnim);
        txt1.setAnimation(topAnim);
        passEt.setAnimation(edittext_anim);
        singin.setAnimation(bottomAnim);
    }

    private void init() {
        singin = findViewById(R.id.signin_BTN);
        passEt = findViewById(R.id.password_ET);
        passEt.setSelected(false);
        auth = FirebaseAuth.getInstance();
        reference = FirebaseDatabase.getInstance().getReference("GuideProfile");
        txt1 = findViewById(R.id.txt1);
        logo = findViewById(R.id.logoG);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }
}
