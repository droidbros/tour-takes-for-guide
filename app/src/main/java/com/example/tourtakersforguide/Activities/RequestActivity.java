package com.example.tourtakersforguide.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.tourtakersforguide.Notifications.OreoNotification;
import com.example.tourtakersforguide.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import es.dmoral.toasty.Toasty;

public class RequestActivity extends AppCompatActivity {
    public static final String TAG = "RequestActivity";
    private TextView startDateTV, returnDateTV;
    private ExtendedFloatingActionButton acceptBTN, rejectBTN;
    private FirebaseAuth auth;
    private DatabaseReference onlineRef;
    private Vibrator vibrator;
    private Ringtone defaultRingtone;
    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calling);
        auth = FirebaseAuth.getInstance();
        Intent intent = getIntent();
        Log.d(TAG, "onCreate: " + intent.getStringExtra("location"));
        onlineRef = FirebaseDatabase.getInstance().getReference()
                    .child("guidesAreOnline")
                    .child(intent.getStringExtra("location"))
                    .child(auth.getUid())
                    .child("status");
            onlineRef.setValue("requesting");
        startDateTV = findViewById(R.id.startDateTV);
        returnDateTV = findViewById(R.id.returnDateTV);
        acceptBTN = findViewById(R.id.acceptBTN);
        rejectBTN = findViewById(R.id.rejectBTN);
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        int ringerMode = ((AudioManager) getSystemService(AUDIO_SERVICE)).getRingerMode();
        Uri defaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_RINGTONE);
        defaultRingtone = RingtoneManager.getRingtone(getApplicationContext(), defaultRingtoneUri);
        defaultRingtone.play();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            defaultRingtone.setLooping(true);
        }

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {1500, 800, 800, 800};
        if (ringerMode != AudioManager.RINGER_MODE_SILENT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createWaveform(pattern, 0),
                        new AudioAttributes.Builder()
                                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                                .setUsage(AudioAttributes.USAGE_ALARM)
                                .build());
            } else {
                vibrator.vibrate(pattern, 0);
            }
        }
        Intent intent2 = new Intent(getApplicationContext(), RequestActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent2, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            OreoNotification oreoNotification = new OreoNotification(getApplicationContext());
            Notification.Builder builder = oreoNotification.getOreoNotification("Guide Request",
                    "You have a guiding request",
                    pendingIntent,
                    null,
                    String.valueOf(R.drawable.ic_stat_notification),
                    null);
            oreoNotification.getManager().notify(1, builder.build());
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "request")
                    .setSmallIcon(R.drawable.ic_stat_notification)
                    .setContentTitle("Guide Request")
                    .setContentText("You have a guiding request")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(false)
                    .setOngoing(true)
                    .setContentIntent(pendingIntent);
            NotificationManager noti = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            noti.notify(1, builder.build());
        }
        notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        startDateTV.setText(intent.getStringExtra("startDate"));
        returnDateTV.setText(intent.getStringExtra("returnDate"));

        DatabaseReference tourRef = FirebaseDatabase.getInstance().getReference().child("tour").child(intent.getStringExtra("typeID")).child("guideID");
        tourRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists()){
                    if(!auth.getUid().matches((String) snapshot.getValue())){
                        tourRef.removeEventListener(this);
                        notificationManager.cancel(1);
                        onlineRef.setValue("idle");
                        defaultRingtone.stop();
                        vibrator.cancel();
                        finish();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        rejectBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationManager.cancel(1);
                onlineRef.setValue("idle");
                defaultRingtone.stop();
                vibrator.cancel();
                finish();
            }
        });

        acceptBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(intent.getStringExtra("typeFor").matches("tour")){
                    Log.d(TAG, "onClick: tour");
                    DatabaseReference tourRef = FirebaseDatabase.getInstance().getReference().child("tour").child(intent.getStringExtra("typeID")).child("guideID");
                    tourRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){
                                Toasty.info(getApplicationContext(),"Sorry you are little bit late for accepting this tour!",Toasty.LENGTH_LONG).show();
                            }else {
                                DatabaseReference check = FirebaseDatabase.getInstance().getReference().child("tour").child(intent.getStringExtra("typeID"));
                                check.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if(snapshot.exists()){
                                            DatabaseReference statusRef = FirebaseDatabase.getInstance().getReference().child("tour").child(intent.getStringExtra("typeID")).child("status");
                                            statusRef.setValue("◦•●◉✿ ACTIVE ✿◉●•◦");
                                            setUserActivity(intent.getStringExtra("typeID"),intent.getStringExtra("typeFor")+"s");
                                            tourRef.setValue(auth.getUid()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()){
                                                        Intent intent1 = new Intent(RequestActivity.this,MainActivity.class)
                                                                .putExtra("forView",intent.getStringExtra("typeFor"))
                                                                .putExtra("startDate",intent.getStringExtra("startDate"))
                                                                .putExtra("returnDate",intent.getStringExtra("returnDate"));
                                                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        notificationManager.cancel(1);
                                                        onlineRef.setValue("idle");
                                                        defaultRingtone.stop();
                                                        vibrator.cancel();
                                                        startActivity(intent1);
                                                        finish();
                                                    }
                                                }
                                            });
                                        }else {
                                            Toasty.info(getApplicationContext(),"Sorry..Tourist cancel this tour..!",Toasty.LENGTH_LONG).show();
                                            notificationManager.cancel(1);
                                            onlineRef.setValue("idle");
                                            defaultRingtone.stop();
                                            vibrator.cancel();
                                            finish();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }else if(intent.getStringExtra("typeFor").matches("event")){
                    Log.d(TAG, "onClick: event");
                    DatabaseReference tourRef = FirebaseDatabase.getInstance().getReference().child("event").child(intent.getStringExtra("typeID")).child("guideID");
                    tourRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if(snapshot.exists()){
                                Toasty.info(getApplicationContext(),"Sorry you are little bit late for accepting this event!",Toasty.LENGTH_LONG).show();
                                notificationManager.cancel(1);
                                onlineRef.setValue("idle");
                                defaultRingtone.stop();
                                vibrator.cancel();
                                finish();
                            }else {
                                DatabaseReference check = FirebaseDatabase.getInstance().getReference().child("event").child(intent.getStringExtra("typeID"));
                                check.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        if(snapshot.exists()){
                                            DatabaseReference statusRef = FirebaseDatabase.getInstance().getReference().child("event").child(intent.getStringExtra("typeID")).child("status");
                                            statusRef.setValue("◦•●◉✿ ACTIVE ✿◉●•◦");
                                            setUserActivity(intent.getStringExtra("typeID"),intent.getStringExtra("typeFor")+"s");
                                            tourRef.setValue(auth.getUid()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()){
                                                        Intent intent1 = new Intent(RequestActivity.this,MainActivity.class)
                                                                .putExtra("forView",intent.getStringExtra("typeFor"))
                                                                .putExtra("startDate",intent.getStringExtra("startDate"))
                                                                .putExtra("returnDate",intent.getStringExtra("returnDate"));
                                                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        notificationManager.cancel(1);
                                                        onlineRef.setValue("idle");
                                                        defaultRingtone.stop();
                                                        vibrator.cancel();
                                                        startActivity(intent1);
                                                        finish();
                                                    }
                                                }
                                            });
                                        }else {
                                            Toasty.info(getApplicationContext(),"Sorry..Tourist cancel this tour..!",Toasty.LENGTH_LONG).show();
                                            notificationManager.cancel(1);
                                            onlineRef.setValue("idle");
                                            defaultRingtone.stop();
                                            vibrator.cancel();
                                            finish();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }
        });

        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                notificationManager.cancel(1);
                defaultRingtone.stop();
                vibrator.cancel();
                finish();
                onlineRef.setValue("idle");
            }
        };
        handler.postDelayed(runnable, 60000);
    }

    private void setUserActivity(String ID, String type) {
        if(type.matches("tours")){
            DatabaseReference userActivityRef = FirebaseDatabase.getInstance().getReference()
                    .child("userActivities")
                    .child(auth.getUid())
                    .child(type);
            String id = userActivityRef.push().getKey();
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("tourID", ID);
            userActivityRef.child(id).setValue(hashMap);
        }else if(type.matches("events")) {
            DatabaseReference userActivityRef = FirebaseDatabase.getInstance().getReference()
                    .child("userActivities")
                    .child(auth.getUid())
                    .child(type);
            String id = userActivityRef.push().getKey();
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("eventID", ID);
            userActivityRef.child(id).setValue(hashMap);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) ||
                (keyCode == KeyEvent.KEYCODE_VOLUME_UP ||
                        (keyCode == KeyEvent.KEYCODE_VOLUME_MUTE))) {
            defaultRingtone.stop();
            vibrator.cancel();
        }
        return true;
    }

    @Override
    public void onBackPressed() {

    }
}