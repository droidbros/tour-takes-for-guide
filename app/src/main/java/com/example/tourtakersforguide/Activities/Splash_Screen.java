package com.example.tourtakersforguide.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tourtakersforguide.R;

import java.util.Locale;
import java.util.jar.Manifest;

public class Splash_Screen extends AppCompatActivity {

    Animation topAnim,bottomAnim,leftAnim,rightAnim,ball1Anim,ball2Anim,ball3Anim;
    private TextView tourtv,ball1,ball2,ball3;
    private ImageView logo;

    private  static int splash_time_out=2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);

        init();

        loadLocale();
        topAnim= AnimationUtils.loadAnimation(this,R.anim.top_animation);
        bottomAnim= AnimationUtils.loadAnimation(this,R.anim.bottom_animation);
        leftAnim= AnimationUtils.loadAnimation(this,R.anim.left_animation);
        rightAnim= AnimationUtils.loadAnimation(this,R.anim.right_animation);
        ball1Anim=AnimationUtils.loadAnimation(this,R.anim.ball1_animation);
        ball2Anim=AnimationUtils.loadAnimation(this,R.anim.ball2_animation);
        ball3Anim=AnimationUtils.loadAnimation(this,R.anim.ball3_animation);

        tourtv.setAnimation(bottomAnim);
        ball1.setAnimation(ball1Anim);
        ball2.setAnimation(ball2Anim);
        ball3.setAnimation(ball3Anim);
        logo.setAnimation(topAnim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent myintent=new Intent(Splash_Screen.this, SignIn.class);
                startActivity(myintent);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finish();
            }
        },splash_time_out);
    }

    private void init() {
        tourtv=findViewById(R.id.tourtv);
        ball1=findViewById(R.id.ball1);
        ball2=findViewById(R.id.ball2);
        logo=findViewById(R.id.logoIV);
        ball3=findViewById(R.id.ball3);
    }

    private void loadLocale() {
        SharedPreferences preferences = getSharedPreferences("com.example.tourtakersforguide.Activities", Context.MODE_PRIVATE);
        String language = preferences.getString("My_Lang", "");

        if (language.equals("en")) {
            setLanguage("en");

        } else if (language.equals("bn")) {
            setLanguage("bn");
        }

    }

    private void setLanguage(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

}
