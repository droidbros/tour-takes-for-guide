package com.example.tourtakersforguide.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.TestLooperManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tourtakersforguide.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class ShowCash extends AppCompatActivity {

    private float days;
    private String eventId,earnings;
    private TextView amount;
    private Button collectBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_cash);

        Intent intent = getIntent();
        days = intent.getFloatExtra("days",0);
        eventId = intent.getStringExtra("id");

        amount = findViewById(R.id.amount);
        collectBtn = findViewById(R.id.collectBtn);

        amount.setText("BDT "+days*500);

        collectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earnings = amount.getText().toString();

                DatabaseReference earnRef = FirebaseDatabase.getInstance().getReference().child("Earnings").child(FirebaseAuth.getInstance()
                        .getCurrentUser().getUid()).child(eventId);

                HashMap<String, Object> earnInfo = new HashMap<>();
                earnInfo.put("amount",earnings);
                earnInfo.put("days",String.valueOf(days));
                earnRef.setValue(earnInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent intent1 = new Intent(ShowCash.this,MainActivity.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent1);
                        finish();
                    }
                });

            }
        });

    }
}