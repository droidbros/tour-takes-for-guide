package com.example.tourtakersforguide.Model;

public class Profile {

    private String name;
    private String email;
    private String password;
    private String address;
    private String phone;
    private String image;
    private float rating;
    private int ratingCounter;
    private int tour;
    private int event;


    public Profile(String name, String email, String password, String address, String phone,
                   String image, float rating, int ratingCounter, int tour, int event) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.address = address;
        this.phone = phone;
        this.image = image;
        this.rating = rating;
        this.ratingCounter = ratingCounter;
        this.tour = tour;
        this.event = event;
    }


    public Profile() {
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getImage() {
        return image;
    }

    public float getRating() {
        return rating;
    }

    public int getRatingCounter() {
        return ratingCounter;
    }

    public int getTour() {
        return tour;
    }

    public int getEvent() {
        return event;
    }
}
