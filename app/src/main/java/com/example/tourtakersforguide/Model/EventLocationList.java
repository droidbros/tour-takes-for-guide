package com.example.tourtakersforguide.Model;

public class EventLocationList {
    private String location;

    public EventLocationList(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }
}
