package com.example.tourtakersforguide.Model;

public class Compliment_model {

    private int image;
    private String text;

    public Compliment_model(int image, String text) {
        this.image = image;
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public String getText() {
        return text;
    }
}
