package com.example.tourtakersforguide.Model;

public class ServiceList {
    private String id;
    private String startingDate;
    private String endDate;
    private String location;
    private String customerJoined;

    public ServiceList(String id, String startingDate, String endDate, String location, String customerJoined) {
        this.id = id;
        this.startingDate = startingDate;
        this.endDate = endDate;
        this.location = location;
        this.customerJoined = customerJoined;
    }

    public ServiceList() {
    }

    public String getId() {
        return id;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getLocation() {
        return location;
    }

    public String getCustomerJoined() {
        return customerJoined;
    }
}
