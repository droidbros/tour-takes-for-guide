package com.example.tourtakersforguide.ForWeather;

import com.google.gson.annotations.SerializedName;

public class HourlyWeatherForecast {
    @SerializedName("id")
    public int id;
    @SerializedName("main")
    public String main;
    @SerializedName("description")
    public String description;
    @SerializedName("icon")
    public String icon;
}
