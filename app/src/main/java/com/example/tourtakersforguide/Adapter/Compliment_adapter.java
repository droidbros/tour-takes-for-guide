package com.example.tourtakersforguide.Adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tourtakersforguide.Model.Compliment_model;
import com.example.tourtakersforguide.R;

import java.util.List;

public class Compliment_adapter extends RecyclerView.Adapter<Compliment_adapter.ViewHolder> {
    private Context context;
    private List<Compliment_model> compliment_models;

    public Compliment_adapter(Context context, List<Compliment_model> compliment_models) {
        this.context = context;
        this.compliment_models = compliment_models;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.compliment_model_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Compliment_model model = compliment_models.get(position);

        holder.compliment_image.setImageResource(model.getImage());
        holder.compliment_text.setText(model.getText());

    }

    @Override
    public int getItemCount() {
        return compliment_models.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView compliment_image;
        private TextView compliment_text;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            compliment_image = itemView.findViewById(R.id.compliment_image);
            compliment_text = itemView.findViewById(R.id.compliment_text);
        }
    }
}
