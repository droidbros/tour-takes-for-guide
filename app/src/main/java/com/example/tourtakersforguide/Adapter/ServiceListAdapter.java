package com.example.tourtakersforguide.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tourtakersforguide.Model.ServiceList;
import com.example.tourtakersforguide.R;

import java.util.List;

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.ViewHolder> {

    private List<ServiceList> serviceLists;
    private Context context;
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ServiceList service = serviceLists.get(position);

    }

    @Override
    public int getItemCount() {
        return serviceLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView startingDate,endDate,location,customerJoined;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            startingDate = itemView.findViewById(R.id.startingDate);
            endDate = itemView.findViewById(R.id.endingDate);
            location = itemView.findViewById(R.id.service_place);
            customerJoined = itemView.findViewById(R.id.service_place);

        }
    }
}
