package com.example.tourtakersforguide.Adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.request.RequestOptions;
import com.example.tourtakersforguide.Activities.GuideChatBox;
import com.example.tourtakersforguide.Activities.ShowCash;
import com.example.tourtakersforguide.GlideApp;
import com.example.tourtakersforguide.Model.Event;
import com.example.tourtakersforguide.Model.EventLocationList;
import com.example.tourtakersforguide.R;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.animations.DescriptionAnimation;
import com.glide.slider.library.slidertypes.TextSliderView;
import com.glide.slider.library.transformers.BaseTransformer;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class OnGoingTourAdapter extends PagerAdapter {
    public static final String TAG = "OnGoingTourAdapter";
    private List<Event> data;
    private Context context;
    private String forView;
    private List<EventLocationList> locationLists;
    private EventLocationListAdapter locationAdapter;
    private View itemView;
    private FirebaseAuth auth;

    public OnGoingTourAdapter(List<Event> data, Context context, String forView) {
        this.data = data;
        this.context = context;
        this.forView = forView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Event event = data.get(position);
        auth = FirebaseAuth.getInstance();
        LayoutInflater mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemView = mLayoutInflater.inflate(R.layout.ongoing_viewpager_layout, container, false);
        ArrayList<String> locationWillbeVisit = new ArrayList<>();

        TextView event_placeTV, event_publish_dateTV, start_dateTV, return_dateTV, meeting_placeTV, touristName, txt20,tourStatus;
        RecyclerView locationRecycleView;
        SliderLayout imageSlider;
        CircleImageView touristImage;
        RelativeLayout relative20;
        Button cancelBTN,startserviceBTN,endserviceBTN;

        event_placeTV = itemView.findViewById(R.id.event_placeTV);
        event_publish_dateTV = itemView.findViewById(R.id.event_publish_dateTV);
        start_dateTV = itemView.findViewById(R.id.start_dateTV);
        return_dateTV = itemView.findViewById(R.id.return_dateTV);
        tourStatus = itemView.findViewById(R.id.tourStatus);
        meeting_placeTV = itemView.findViewById(R.id.meeting_placeTV);
        locationRecycleView = itemView.findViewById(R.id.eventLocationList_recyclerview);
        locationRecycleView.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        locationRecycleView.setHasFixedSize(true);
        imageSlider = itemView.findViewById(R.id.sliderFromEventDetails);
        relative20 = itemView.findViewById(R.id.relative20);
        touristName = itemView.findViewById(R.id.tour_guide_nameTV);
        touristImage = itemView.findViewById(R.id.tour_guide_imageIV);
        cancelBTN = itemView.findViewById(R.id.cencelTourBTN);
        startserviceBTN = itemView.findViewById(R.id.startserviceBTN);
        endserviceBTN = itemView.findViewById(R.id.endserviceBTN);
        txt20 = itemView.findViewById(R.id.txt20);
        String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        if (forView.matches("event")) {
            cancelBTN.setText("Cancel Event");

            DatabaseReference statusRef = FirebaseDatabase.getInstance().getReference("event").child(event.getId());
            statusRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()){
                        String status = snapshot.child("status").getValue().toString();
                        tourStatus.setText(status);
                        if (status.equals("◦•●◉✿ RUNNING ✿◉●•◦")){
                            startserviceBTN.setVisibility(View.GONE);
                        }else if (status.equals("◦•●◉✿ END ✿◉●•◦")){
                            endserviceBTN.setVisibility(View.GONE);
                            cancelBTN.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

            DatabaseReference locationListRef = FirebaseDatabase.getInstance().getReference().child("eventLocationList").child(event.getId());
            locationListRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        locationLists = new ArrayList<>();
                        for (DataSnapshot childSnap : snapshot.getChildren()) {
                            locationWillbeVisit.add(childSnap.getKey());
                            EventLocationList location = new EventLocationList(childSnap.getKey());
                            locationLists.add(location);
                        }
                        locationAdapter = new EventLocationListAdapter(locationLists, itemView.getContext());
                        locationRecycleView.setAdapter(locationAdapter);

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("location").child(event.getPlace().toLowerCase());
                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    for (DataSnapshot childSnap2 : snapshot.getChildren()) {
                                        HashMap<String, Object> map = (HashMap<String, Object>) childSnap2.getValue();
                                        if (locationWillbeVisit.contains(map.get("locationName").toString())) {
                                            RequestOptions requestOptions = new RequestOptions();
                                            requestOptions.centerCrop();
                                            TextSliderView sliderView = new TextSliderView(itemView.getContext());
                                            sliderView
                                                    .image((String) map.get("image"))
                                                    .description((String) map.get("locationName"))
                                                    .setRequestOption(requestOptions)
                                                    .setProgressBarVisible(false);

                                            sliderView.bundle(new Bundle());
                                            sliderView.getBundle().putString("extra", (String) map.get("locationName"));
                                            imageSlider.addSlider(sliderView);
                                            if (imageSlider.getSliderImageCount() < 2) {
                                                imageSlider.stopAutoCycle();
                                                imageSlider.setPagerTransformer(false, new BaseTransformer() {
                                                    @Override
                                                    protected void onTransform(View view, float v) {
                                                    }
                                                });
                                                imageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Top);
                                            } else {

                                                imageSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
                                                imageSlider.startAutoCycle();
                                                imageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Top);
                                                imageSlider.setCustomAnimation(new DescriptionAnimation());
                                                imageSlider.setDuration(4000);
                                                imageSlider.stopCyclingWhenTouch(true);

                                            }
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        } else if (forView.matches("tour")) {
            cancelBTN.setText("Cancel Tour");

            DatabaseReference statusRef = FirebaseDatabase.getInstance().getReference("tour").child(event.getId());
            statusRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()){
                        String status = snapshot.child("status").getValue().toString();
                        if (status.equals("◦•●◉✿ RUNNING ✿◉●•◦")){
                            startserviceBTN.setVisibility(View.GONE);
                            cancelBTN.setVisibility(View.GONE);
                        }else if (status.equals("◦•●◉✿ END ✿◉●•◦")){
                            endserviceBTN.setVisibility(View.GONE);
                            cancelBTN.setVisibility(View.GONE);
                        }
                        tourStatus.setText(status);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });


            DatabaseReference locationListRef = FirebaseDatabase.getInstance().getReference().child("tourLocationList").child(event.getId());
            locationListRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        locationLists = new ArrayList<>();
                        for (DataSnapshot childSnap : snapshot.getChildren()) {
                            locationWillbeVisit.add(childSnap.getKey());
                            EventLocationList location = new EventLocationList(childSnap.getKey());
                            locationLists.add(location);
                        }
                        locationAdapter = new EventLocationListAdapter(locationLists, itemView.getContext());
                        locationRecycleView.setAdapter(locationAdapter);

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("location").child(event.getPlace().toLowerCase());
                        ref.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    for (DataSnapshot childSnap2 : snapshot.getChildren()) {
                                        HashMap<String, Object> map = (HashMap<String, Object>) childSnap2.getValue();
                                        if (locationWillbeVisit.contains(map.get("locationName").toString())) {
                                            RequestOptions requestOptions = new RequestOptions();
                                            requestOptions.centerCrop();
                                            TextSliderView sliderView = new TextSliderView(itemView.getContext());
                                            sliderView
                                                    .image((String) map.get("image"))
                                                    .description((String) map.get("locationName"))
                                                    .setRequestOption(requestOptions)
                                                    .setProgressBarVisible(false);

                                            sliderView.bundle(new Bundle());
                                            sliderView.getBundle().putString("extra", (String) map.get("locationName"));
                                            imageSlider.addSlider(sliderView);
                                            if (imageSlider.getSliderImageCount() < 2) {
                                                imageSlider.stopAutoCycle();
                                                imageSlider.setPagerTransformer(false, new BaseTransformer() {
                                                    @Override
                                                    protected void onTransform(View view, float v) {
                                                    }
                                                });
                                                imageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Top);
                                            } else {

                                                imageSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
                                                imageSlider.startAutoCycle();
                                                imageSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Top);
                                                imageSlider.setCustomAnimation(new DescriptionAnimation());
                                                imageSlider.setDuration(4000);
                                                imageSlider.stopCyclingWhenTouch(true);

                                            }
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

        event_placeTV.setText(event.getPlace());
        event_publish_dateTV.setText(event.getPublishDate());
        start_dateTV.setText(event.getStartDate());
        return_dateTV.setText(event.getReturnDate());
        meeting_placeTV.setText(event.getGuideMeetPlace());
        if (event.getStartDate().equals(currentDate)){
            startserviceBTN.setVisibility(View.VISIBLE);
        }
        if (event.getReturnDate().equals(currentDate)){
            endserviceBTN.setVisibility(View.VISIBLE);
        }

        DatabaseReference adminRef = FirebaseDatabase.getInstance().getReference().child("profile").child(event.getEventPublisherId());
        adminRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    HashMap<String, Object> map = (HashMap<String, Object>) snapshot.getValue();
                    touristName.setText((String) map.get("name"));
                    if (forView.matches("tour")) {
                        txt20.setText("Tourist (Tap to view profile)");
                    } else if (forView.matches("event")) {
                        txt20.setText("Event Admin (Tap to view profile)");
                    }
                    if (!map.get("image").toString().matches("")) {
                        try {
                            GlideApp.with(itemView.getContext())
                                    .load(map.get("image"))
                                    .fitCenter()
                                    .into(touristImage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (map.get("sex").toString().matches("male")) {
                            try {
                                GlideApp.with(itemView.getContext())
                                        .load(getImageFromDrawable("man"))
                                        .centerInside()
                                        .into(touristImage);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (map.get("sex").toString().matches("female")) {
                            try {
                                GlideApp.with(itemView.getContext())
                                        .load(getImageFromDrawable("woman"))
                                        .centerInside()
                                        .into(touristImage);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    relative20.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ShowProfilePopup((String) map.get("name"),
                                    (String) map.get("phone"),
                                    (String) map.get("email"),
                                    (String) map.get("image"),
                                    (String) map.get("sex"),
                                    (Long) map.get("event"),
                                    (Long) map.get("tour"),
                                    (Long) map.get("ratingCounter"),
                                    (Long) map.get("rating"),
                                    event.getId(),
                                    event.getEventPublisherId());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        startserviceBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference startRef = FirebaseDatabase.getInstance().getReference().child("OnGoingTour").child(event.getId());
                HashMap<String, Object> onGoingInfo = new HashMap<>();
                onGoingInfo.put("eventId",event.getId());
                onGoingInfo.put("startDate",currentDate);
                onGoingInfo.put("publisherId",event.getEventPublisherId());
                onGoingInfo.put("guideId",event.getGuideID());
                onGoingInfo.put("returnDate",event.getReturnDate());
                startRef.setValue(onGoingInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (forView.matches("tour")) {
                            DatabaseReference updateRef = FirebaseDatabase.getInstance().getReference("tour").child(event.getId());
                            updateRef.child("status").setValue("◦•●◉✿ RUNNING ✿◉●•◦");
                        } else if (forView.matches("event")) {
                            DatabaseReference updateRef = FirebaseDatabase.getInstance().getReference("event").child(event.getId());
                            updateRef.child("status").setValue("◦•●◉✿ RUNNING ✿◉●•◦");
                        }
                        Toast.makeText(context, "Sevice Start!", Toast.LENGTH_SHORT).show();
                        startserviceBTN.setVisibility(View.GONE);
                    }
                });

            }
        });

        endserviceBTN.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
               String startDate = event.getStartDate();
                SimpleDateFormat myFormat = new SimpleDateFormat("dd/MM/yyyy");

                try {
                    Date dateBefore = myFormat.parse(startDate);
                    Date dateAfter = myFormat.parse(currentDate);

                    long difference = dateAfter.getTime() - dateBefore.getTime();
                    float daysBetween = (difference / (1000*60*60*24));
                    if (forView.matches("tour")) {
                        DatabaseReference updateRef = FirebaseDatabase.getInstance().getReference("tour").child(event.getId());
                        updateRef.child("status").setValue("◦•●◉✿ END ✿◉●•◦");
                    } else if (forView.matches("event")) {
                        DatabaseReference updateRef = FirebaseDatabase.getInstance().getReference("event").child(event.getId());
                        updateRef.child("status").setValue("◦•●◉✿ END ✿◉●•◦");
                    }
                    context.startActivity(new Intent(context, ShowCash.class).putExtra("days",daysBetween)
                            .putExtra("id",event.getId()));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        });

        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelAlertDialog(event.getId(), forView);
            }
        });

        container.addView(itemView);

        return itemView;
    }

    private void cancelAlertDialog(String ID, String forView) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Alert..!!");
        dialog.setIcon(R.drawable.ic_leave_white);
        dialog.setMessage("Do you want to cancel this " + forView + "?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ArrayList<String> eventKeys = new ArrayList<>();
                if (forView.matches("tour")) {
                    DatabaseReference tourRef = FirebaseDatabase.getInstance().getReference().child("tour").child(ID).child("guideID");
                    tourRef.removeValue();
                    DatabaseReference removeActivity = FirebaseDatabase.getInstance().getReference().child("userActivities").child(auth.getUid()).child("tours");
                    removeActivity.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                                    //eventKeys.add(childSnapshot.getKey());
                                    HashMap<String, Object> hashMap = (HashMap<String, Object>) childSnapshot.getValue();
                                    if (hashMap.get("tourID").toString().equals(ID)) {
                                        eventKeys.add(childSnapshot.getKey());
                                        //Log.d(TAG, "onDataChange: "+childSnapshot.getKey());
                                    }
                                }
                                for (int i = 0; i < eventKeys.size(); i++) {
                                    DatabaseReference remove = FirebaseDatabase.getInstance().getReference().child("userActivities").child(auth.getUid()).child("tours").child(eventKeys.get(i));
                                    remove.removeValue();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    DatabaseReference statusRef = FirebaseDatabase.getInstance().getReference().child("tour").child(ID).child("status");
                    statusRef.setValue("◦•●◉✿ Pending ✿◉●•◦");
                } else if (forView.matches("event")) {
                    DatabaseReference tourRef = FirebaseDatabase.getInstance().getReference().child("event").child(ID).child("guideID");
                    tourRef.removeValue();
                    DatabaseReference removeActivity = FirebaseDatabase.getInstance().getReference().child("userActivities").child(auth.getUid()).child("events");
                    removeActivity.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                                    //eventKeys.add(childSnapshot.getKey());
                                    HashMap<String, Object> hashMap = (HashMap<String, Object>) childSnapshot.getValue();
                                    if (hashMap.get("eventID").toString().equals(ID)) {
                                        eventKeys.add(childSnapshot.getKey());
                                        //Log.d(TAG, "onDataChange: "+childSnapshot.getKey());
                                    }
                                }
                                for (int i = 0; i < eventKeys.size(); i++) {
                                    DatabaseReference remove = FirebaseDatabase.getInstance().getReference().child("userActivities").child(auth.getUid()).child("events").child(eventKeys.get(i));
                                    remove.removeValue();
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    DatabaseReference statusRef = FirebaseDatabase.getInstance().getReference().child("event").child(ID).child("status");
                    statusRef.setValue("◦•●◉✿ Pending ✿◉●•◦");
                }
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();


    }

    @SuppressLint("ClickableViewAccessibility")
    public void ShowProfilePopup(String member_name, String member_phone, String member_email, String member_image, String member_gender,
                                 long event, long tour, long ratingCounter, long rating, String eventID, String memberID) {
        TextView namePopUp, phonePopUp, emailPopUp, eventPopUp, tourPopUp, ratingPopUp;
        ImageView pic;
        Button callBTN, textBTN;
        CircleImageView close;
        Dialog profileDialog = new Dialog(context);
        profileDialog.setContentView(R.layout.profile_popup);
        close = profileDialog.findViewById(R.id.close);
        namePopUp = profileDialog.findViewById(R.id.namePopUp);
        phonePopUp = profileDialog.findViewById(R.id.phonePopUp);
        emailPopUp = profileDialog.findViewById(R.id.emailPopUp);
        eventPopUp = profileDialog.findViewById(R.id.eventPopUp);
        tourPopUp = profileDialog.findViewById(R.id.tourPopUp);
        ratingPopUp = profileDialog.findViewById(R.id.ratingPopUp);
        pic = profileDialog.findViewById(R.id.pic);
        callBTN = profileDialog.findViewById(R.id.callBTN);
        textBTN = profileDialog.findViewById(R.id.textBTN);

        DecimalFormat df = new DecimalFormat("0.0");
        double averageRating = 0;
        if(rating!=0){
            averageRating = rating / ratingCounter;
        }

        GlideApp.with(profileDialog.getContext())
                .load(getImageFromDrawable("ic_close"))
                .centerCrop()
                .into(close);


        callBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        callIntent.setData(Uri.parse("tel:" + member_phone));
                        context.startActivity(callIntent);
                    }
                };
                handler.postDelayed(runnable, 200);
            }
        });

        textBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        Intent chatIntent = new Intent(context, GuideChatBox.class);
                        chatIntent.putExtra("chatPartnerID", memberID);
                        chatIntent.putExtra("eventId", eventID);
                        context.startActivity(chatIntent);
                        ((FragmentActivity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                };
                handler.postDelayed(runnable, 200);
            }
        });

        namePopUp.setText(member_name);
        phonePopUp.setText(member_phone);
        emailPopUp.setText(member_email);
        eventPopUp.setText(String.valueOf(event));
        tourPopUp.setText(String.valueOf(tour));
        ratingPopUp.setText(String.valueOf(df.format(averageRating)));
        if (!member_image.matches("")) {
            GlideApp.with(profileDialog.getContext())
                    .load(member_image)
                    .fitCenter()
                    .into(pic);
        } else {
            if (member_gender != null) {
                if (member_gender.matches("male")) {
                    GlideApp.with(profileDialog.getContext())
                            .load(getImageFromDrawable("man"))
                            .fitCenter()
                            .into(pic);
                } else if (member_gender.matches("female")) {
                    GlideApp.with(profileDialog.getContext())
                            .load(getImageFromDrawable("woman"))
                            .fitCenter()
                            .into(pic);
                }
            } else {
                GlideApp.with(profileDialog.getContext())
                        .load(getImageFromDrawable("man"))
                        .fitCenter()
                        .into(pic);
            }
        }
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileDialog.dismiss();
            }
        });
        profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        profileDialog.setCanceledOnTouchOutside(false);
        profileDialog.show();
    }

    public int getImageFromDrawable(String imageName) {

        int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());

        return drawableResourceId;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void startUpdate(ViewGroup container) {
        super.startUpdate(container);
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        super.finishUpdate(container);
    }

    static class InternetCheck extends AsyncTask<Void, Void, Boolean> {

        private Consumer mConsumer;

        public interface Consumer {
            void accept(Boolean internet);
        }

        public InternetCheck(Consumer consumer) {
            mConsumer = consumer;
            execute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Socket sock = new Socket();
                sock.connect(new InetSocketAddress("8.8.8.8", 53), 1500);
                sock.close();
                return true;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean internet) {
            mConsumer.accept(internet);
        }
    }

}
