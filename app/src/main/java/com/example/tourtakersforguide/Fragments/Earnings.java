package com.example.tourtakersforguide.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.example.tourtakersforguide.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Earnings extends Fragment {
    private ImageView tour_nav_icon;
    private DrawerLayout tDrawerLayout;

    public Earnings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_earnings, container, false);
        init(view);
        tDrawerLayout = getActivity().findViewById(R.id.drawer_layout);
        tour_nav_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tDrawerLayout.openDrawer(GravityCompat.START);
                hideKeyboardFrom(view.getContext());
            }
        });
        return view;
    }

    private void init(View view) {
        tour_nav_icon = view.findViewById(R.id.nav_icon);
    }

    private void hideKeyboardFrom(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
}
