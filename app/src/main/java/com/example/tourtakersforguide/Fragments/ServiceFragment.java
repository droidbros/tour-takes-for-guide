package com.example.tourtakersforguide.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.tourtakersforguide.R;

public class ServiceFragment extends Fragment {

    private ImageView setting_nav_icon;
    private DrawerLayout sDrawerLayout;

    public ServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        init(view);
        sDrawerLayout = getActivity().findViewById(R.id.drawer_layout);
        setting_nav_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        return view;
    }

    private void init(View view) {
        setting_nav_icon = view.findViewById(R.id.setting_nav_icon);
    }


}
