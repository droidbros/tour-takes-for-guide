package com.example.tourtakersforguide.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.example.tourtakersforguide.Adapter.OnGoingTourAdapter;
import com.example.tourtakersforguide.Model.Event;
import com.example.tourtakersforguide.R;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.polyak.iconswitch.IconSwitch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TourFragment extends Fragment {
    public static final String TAG = "TourFragment";
    private ImageView tour_nav_icon;
    private DrawerLayout tDrawerLayout;
    private ExtendedFloatingActionButton onlineBTN;
    private FirebaseUser user;
    public String userID, userLocation, forView, startDate, returnDate;
    private Boolean online;
    private FusedLocationProviderClient mFusedLocationClient;
    private OnGoingTourAdapter adapter;
    private ViewPager viewPager;
    private List<Event> eventList;
    private IconSwitch iconSwitch;
    private Animation anim;
    private TextView tourTitle;
    private LottieAnimationView emptyAmin;
    private View view;
    private boolean changePermission = true;

    public TourFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tour, container, false);
        init(view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            forView = bundle.getString("forView");
            startDate = bundle.getString("startDate");
            returnDate = bundle.getString("returnDate");
        }
        Log.d(TAG, "onCreateView: " + bundle);
        firstLoad();
        getGuideInfo(new guideInfoCallback() {
            @Override
            public void guideID(String ID) {
                userID = ID;
            }

            @Override
            public void guideLocation(String location) {
                userLocation = location.replaceAll("[^a-zA-Z\\s]", "");
            }
        });
        tDrawerLayout = getActivity().findViewById(R.id.drawer_layout);

        tour_nav_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tDrawerLayout.openDrawer(GravityCompat.START);
                hideKeyboardFrom(view.getContext());
            }
        });

        iconSwitch.setCheckedChangeListener(new IconSwitch.CheckedChangeListener() {
            @Override
            public void onCheckChanged(IconSwitch.Checked current) {
                switch (iconSwitch.getChecked()) {
                    case LEFT:
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                                tourTitle.setText("Tour");
                            }
                        });
                        tourTitle.startAnimation(anim);
                        if(changePermission) {
                            onGoingTourLoad(0, startDate, returnDate);
                        }else {
                            changePermission = true;
                        }

                        break;
                    case RIGHT:
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                                tourTitle.setText("Event");
                            }
                        });
                        tourTitle.startAnimation(anim);
                        if(changePermission) {
                            onGoingEventLoad(0, startDate, returnDate);
                        }else {
                            changePermission = true;
                        }

                        break;
                }
            }
        });

        DatabaseReference eventIDRef = FirebaseDatabase.getInstance().getReference().child("userActivities").child(user.getUid()).child("tours");
        eventIDRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    iconSwitch.setChecked(IconSwitch.Checked.LEFT);
                } else {
                    DatabaseReference tourIDRef = FirebaseDatabase.getInstance().getReference().child("userActivities").child(user.getUid()).child("events");
                    tourIDRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()) {
                                iconSwitch.setChecked(IconSwitch.Checked.RIGHT);
                            } else {
                                iconSwitch.setChecked(IconSwitch.Checked.LEFT);
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        onlineBTN.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                if (online) {
                    DatabaseReference onlineRef = FirebaseDatabase.getInstance().getReference().child("guidesAreOnline").child(userLocation.substring(0,1).toUpperCase()+userLocation.substring(1).toLowerCase()).child(userID);
                    onlineRef.removeValue();
                } else {
                    mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            DatabaseReference onlineRef = FirebaseDatabase.getInstance().getReference().child("guidesAreOnline").child(userLocation.substring(0,1).toUpperCase()+userLocation.substring(1).toLowerCase()).child(userID);
                            HashMap<String, Object> hashMap = new HashMap<>();
                            hashMap.put("ID", userID);
                            hashMap.put("currentLocationLatitude", location.getLatitude());
                            hashMap.put("currentLocationLongitude", location.getLongitude());
                            hashMap.put("status", "idle");
                            onlineRef.setValue(hashMap);
                        }
                    });
                }
            }
        });
        return view;
    }

    private void firstLoad() {
        if (forView != null) {
            if (forView.matches("tour")) {
                changePermission = false;
                iconSwitch.setChecked(IconSwitch.Checked.LEFT);
                onGoingTourLoad(0,startDate,returnDate);
            } else if (forView.matches("event")) {
                changePermission = false;
                iconSwitch.setChecked(IconSwitch.Checked.RIGHT);
                onGoingEventLoad(0,startDate,returnDate);
            }
        }else {
            DatabaseReference eventIDRef = FirebaseDatabase.getInstance().getReference().child("userActivities").child(user.getUid()).child("tours");
            eventIDRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    eventList = new ArrayList<>();
                    adapter = new OnGoingTourAdapter(eventList, getContext(), "tour");
                    viewPager = (ViewPager) view.findViewById(R.id.onGoingViewPager);
                    viewPager.setAdapter(adapter);
                    ArrayList<String> tourlist = new ArrayList<>();
                    if (snapshot.exists()) {
                        for (DataSnapshot childSnap : snapshot.getChildren()) {
                            HashMap<String, Object> map = (HashMap<String, Object>) childSnap.getValue();
                            tourlist.add((String) map.get("tourID"));
                        }
                        for (int i = 0; i < tourlist.size(); i++) {
                            DatabaseReference tourRef = FirebaseDatabase.getInstance().getReference().child("tour").child(tourlist.get(i));
                            int finalI = i;
                            tourRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if (snapshot.exists()) {
                                        emptyAmin.setVisibility(View.GONE);
                                        HashMap<String, Object> map = (HashMap<String, Object>) snapshot.getValue();
                                        if (map.get("guideID") != null) {
                                            if (map.get("guideID").toString().matches(user.getUid())) {
                                                Event event = snapshot.getValue(Event.class);
                                                changePermission = true;
                                                eventList.add(event);
                                                adapter.notifyDataSetChanged();
                                                viewPager.setOffscreenPageLimit(tourlist.size());
                                            }
                                        } else {
                                            emptyAmin.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        emptyAmin.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    } else {
                        iconSwitch.setChecked(IconSwitch.Checked.RIGHT);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
    }

    private void init(View view) {
        tour_nav_icon = view.findViewById(R.id.nav_icon);
        onlineBTN = view.findViewById(R.id.onlineBTN);
        onlineBTN.shrink();
        user = FirebaseAuth.getInstance().getCurrentUser();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(view.getContext());
        iconSwitch = view.findViewById(R.id.tourSwitch);
        tourTitle = view.findViewById(R.id.tourTitle);
        emptyAmin = view.findViewById(R.id.emptyAmin);
        anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(200);
        anim.setRepeatCount(1);
        anim.setRepeatMode(Animation.REVERSE);
    }

    public void getOnlineStatus() {
        DatabaseReference onlineRef = FirebaseDatabase.getInstance().getReference().child("guidesAreOnline").child(userLocation.substring(0,1).toUpperCase()+userLocation.substring(1).toLowerCase()).child(userID);
        onlineRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    online = true;
                    onlineBTN.setVisibility(View.VISIBLE);
                    onlineBTN.setText("Go Offline");
                    onlineBTN.shrink();
                    onlineBTN.extend();

                } else {
                    online = false;
                    onlineBTN.setVisibility(View.VISIBLE);
                    onlineBTN.setText("Go Online");
                    onlineBTN.shrink();
                    onlineBTN.extend();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void onGoingTourLoad(int position, String startDate, String returnDate) {
        DatabaseReference eventIDRef = FirebaseDatabase.getInstance().getReference().child("userActivities").child(user.getUid()).child("tours");
        eventIDRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                eventList = new ArrayList<>();
                adapter = new OnGoingTourAdapter(eventList, getContext(), "tour");
                viewPager = (ViewPager) view.findViewById(R.id.onGoingViewPager);
                viewPager.setAdapter(adapter);
                ArrayList<String> tourlist = new ArrayList<>();
                if (snapshot.exists()) {
                    for (DataSnapshot childSnap : snapshot.getChildren()) {
                        HashMap<String, Object> map = (HashMap<String, Object>) childSnap.getValue();
                        tourlist.add((String) map.get("tourID"));
                    }
                    for (int i = 0; i < tourlist.size(); i++) {
                        DatabaseReference tourRef = FirebaseDatabase.getInstance().getReference().child("tour").child(tourlist.get(i));
                        int finalI = i;
                        tourRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    emptyAmin.setVisibility(View.GONE);
                                    HashMap<String, Object> map = (HashMap<String, Object>) snapshot.getValue();
                                    if (map.get("guideID") != null) {
                                        if (map.get("guideID").toString().matches(user.getUid())) {
                                            Event event = snapshot.getValue(Event.class);
                                            changePermission = true;
                                            eventList.add(event);
                                            adapter.notifyDataSetChanged();
                                            if (startDate != null && returnDate != null) {
                                                if ((event.getStartDate().matches(startDate) && (event.getReturnDate().matches(returnDate)))) {
                                                    viewPager.setOffscreenPageLimit(tourlist.size());
                                                    viewPager.setCurrentItem(finalI);
                                                    //bundle = null;
                                                } else {
                                                    //Log.d(TAG, "onDataChange: else");
                                                    viewPager.setOffscreenPageLimit(tourlist.size());
                                                    viewPager.setCurrentItem(position);
                                                }
                                            } else {
                                                //Log.d(TAG, "onDataChange: else1");
                                                viewPager.setOffscreenPageLimit(tourlist.size());
                                                viewPager.setCurrentItem(position);
                                            }
                                        }
                                    } else {
                                        emptyAmin.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    emptyAmin.setVisibility(View.VISIBLE);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError error) {

                            }
                        });
                    }
                } else {
                    emptyAmin.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void onGoingEventLoad(int position, String startDate, String returnDate) {
        DatabaseReference eventIDRef = FirebaseDatabase.getInstance().getReference().child("userActivities").child(user.getUid()).child("events");
        eventIDRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.d(TAG, "onDataChange: " + iconSwitch.getChecked());
                if (iconSwitch.getChecked().equals(IconSwitch.Checked.RIGHT)) {
                    eventList = new ArrayList<>();
                    adapter = new OnGoingTourAdapter(eventList, getContext(), "event");
                    viewPager = (ViewPager) view.findViewById(R.id.onGoingViewPager);
                    viewPager.setAdapter(adapter);
                    ArrayList<String> tourlist = new ArrayList<>();
                    if (snapshot.exists()) {
                        for (DataSnapshot childSnap : snapshot.getChildren()) {
                            HashMap<String, Object> map = (HashMap<String, Object>) childSnap.getValue();
                            tourlist.add((String) map.get("eventID"));
                        }
                        for (int i = 0; i < tourlist.size(); i++) {
                            DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference().child("event").child(tourlist.get(i));
                            int finalI = i;
                            eventRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if (snapshot.exists()) {
                                        HashMap<String, Object> map = (HashMap<String, Object>) snapshot.getValue();
                                        if (map.get("guideID") != null) {
                                            if (map.get("guideID").toString().matches(user.getUid())) {
                                                Event event = snapshot.getValue(Event.class);
                                                changePermission = true;
                                                eventList.add(event);
                                                adapter.notifyDataSetChanged();
                                                emptyAmin.setVisibility(View.GONE);
                                                //Log.d(TAG, "onDataChange: if");
                                                if (startDate != null && returnDate != null) {
                                                    if ((event.getStartDate().matches(startDate) && (event.getReturnDate().matches(returnDate)))) {
                                                        viewPager.setOffscreenPageLimit(tourlist.size());
                                                        viewPager.setCurrentItem(finalI);
                                                        //bundle = null;
                                                    } else {
                                                        //Log.d(TAG, "onDataChange: else");
                                                        viewPager.setOffscreenPageLimit(tourlist.size());
                                                        viewPager.setCurrentItem(position);
                                                    }
                                                } else {
                                                    //Log.d(TAG, "onDataChange: else1");
                                                    viewPager.setOffscreenPageLimit(tourlist.size());
                                                    viewPager.setCurrentItem(position);
                                                }
                                            }
                                        } else {
                                            //Log.d(TAG, "onDataChange: else2");
                                            emptyAmin.setVisibility(View.VISIBLE);
                                        }
                                    } else {
                                        //Log.d(TAG, "onDataChange: else3");
                                        emptyAmin.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {

                                }
                            });
                        }
                    } else {
                        //Log.d(TAG, "onDataChange: else4");
                        emptyAmin.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public interface guideInfoCallback {
        void guideID(String ID);

        void guideLocation(String location);
    }

    private void getGuideInfo(guideInfoCallback infoCallback) {
        DatabaseReference infoRef = FirebaseDatabase.getInstance().getReference().child("GuideProfile").child(user.getUid());
        infoRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    HashMap<String, Object> map = (HashMap<String, Object>) dataSnapshot.getValue();
                    infoCallback.guideID((String) map.get("Id"));
                    infoCallback.guideLocation((String) map.get("location"));
                    getOnlineStatus();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void hideKeyboardFrom(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }
}
