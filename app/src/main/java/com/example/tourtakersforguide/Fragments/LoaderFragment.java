package com.example.tourtakersforguide.Fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.airbnb.lottie.LottieAnimationView;
import com.example.tourtakersforguide.R;
import com.google.firebase.auth.FirebaseAuth;

public class LoaderFragment extends Fragment {
    public static final String TAG = "LoaderFragment";
    private LottieAnimationView loadinAmin;
    private FirebaseAuth auth;
    private boolean hasTour;
    private boolean hasEvent;

    public LoaderFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_loader, container, false);
        loadinAmin = view.findViewById(R.id.loadinAmin);
        auth = FirebaseAuth.getInstance();
        Bundle bundle = getArguments();
        loadinAmin.setAnimation("dot_circle_loading.json");
        loadinAmin.playAnimation();
        loadinAmin.addAnimatorListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                try {
                    if (bundle != null) {
                        if ((bundle.getString("startDate") != null) && (bundle.getString("returnDate") != null)) {
                            TourFragment tourFragment = new TourFragment();
                            tourFragment.setArguments(bundle);
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.fragment_container, tourFragment);
                            transaction.commit();
                        }
                    } else {
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment_container, new TourFragment());
                        transaction.commit();
                    }

                } catch (IllegalStateException e) {
                    Log.d(TAG, "onFinish: " + e.getMessage());
                }
            }
        });

        return view;
    }
}
